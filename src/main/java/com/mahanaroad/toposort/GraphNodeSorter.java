package com.mahanaroad.toposort;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;



/**
 * A utility class to sort a Directed Acyclic Graph (DAG) of graph nodes in
 * dependency order. After sorting, each node in the list will have no
 * dependencies on any of the nodes that come after it in the list. This type
 * of sort is called a topological sort.
 * 
 * Instances of this class are not reusable.
 * 
 */
public class GraphNodeSorter<T> {


    private final Set<GraphNode<T>> graphNodes = new HashSet<>();

    private final List<GraphNode<T>> sortedGraphNodes = new LinkedList<>();


    public GraphNodeSorter() {
        // do nothing
    }


    /**
     * @param graphNode Must not be null.
     * @throws IllegalArgumentException if any argument is null.
     */
    public void addNode(GraphNode<T> graphNode) {

        if (graphNode == null) {
            throw new IllegalArgumentException("graphNode must not be null");
        }
        
        this.graphNodes.add(graphNode);
        
    }


    /**
     * Returns a list of nodes where each node in the list has no dependency
     * on any subsequent node.
     * 
     * @return Never null.
     * @throws MissingNodeException
     * @throws CyclicDependencyException
     */
    public List<GraphNode<T>> topologicalSort() {

        checkForMissingNodes();
        sortNodes();
        return this.sortedGraphNodes;

    }


    private void checkForMissingNodes() {

        Set<String> preRequisiteNodes = getMissingPreRequisiteNodes();

        if (!preRequisiteNodes.isEmpty()) {
            throw new MissingNodeException(preRequisiteNodes);
        }

    }


    private void sortNodes() {

        while (this.graphNodes.size() > 0) {
            GraphNode<T> currentNode = findNodeWithNoDependencies();
            this.sortedGraphNodes.add(currentNode);
            deleteNode(currentNode);
        }

    }


    private Set<String> getMissingPreRequisiteNodes() {

        Set<String> nodeIdSet = new HashSet<>();
        Set<String> preRequisiteNodeIdSet = new HashSet<>();

        for (GraphNode<T> graphNode : this.graphNodes) {
            nodeIdSet.add(graphNode.getNodeName());
            preRequisiteNodeIdSet.addAll(graphNode.getDependsOnNodes());
        }

        preRequisiteNodeIdSet.removeAll(nodeIdSet);
        return preRequisiteNodeIdSet;

    }


    private GraphNode<T> findNodeWithNoDependencies() {

        for (GraphNode<T> graphNode : this.graphNodes) {

            if (graphNode.hasNoDependencies()) {
                return graphNode;
            }

        }

        String message = "Nodes = " + this.graphNodes;
        throw new CyclicDependencyException(message);

    }


    private void deleteNode(GraphNode<T> nodeToBeDeleted) {

        Iterator<GraphNode<T>> nodesItr = this.graphNodes.iterator();

        while (nodesItr.hasNext()) {

            GraphNode<T> graphNode = nodesItr.next();

            if (graphNode.equals(nodeToBeDeleted)) {
                nodesItr.remove();
            } else {
                graphNode.removeDependencies(nodeToBeDeleted);
            }

        }

    }

}
