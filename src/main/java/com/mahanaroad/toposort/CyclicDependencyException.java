package com.mahanaroad.toposort;


/**
 * Indicates that the nodes in a {@link GraphNodeSorter} contain a cyclic dependency.
 * 
 */
public class CyclicDependencyException extends RuntimeException {
    
    private static final long serialVersionUID = -6190114220878667447L;


    public CyclicDependencyException() {
        //do nothing
    }
    
    
    public CyclicDependencyException(String message) {
        super(message);
    }

}
